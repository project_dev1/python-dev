from django.urls import path
from . import views
from .views import DevDetailVue
from .views import IndexView

app_name = 'developer'

urlpatterns = [
path('', IndexView.as_view(), name='index'),
path('<int:pk>', DevDetailVue.as_view(), name='detail'),
path('create', views.create, name='create'),
path('specifics/<int:developer_id>', views.delete, name='delete'),




]
