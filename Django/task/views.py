from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import generic
from django.views.generic import ListView

from .forms import TaskForm
from .models import Task


def index(request):
    context = {
        'tasks': Task.objects.all(),
    }
    return render(request, 'task/index.html', context)


class IndexView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    model = Task
    template_name = "task/index.html"
    context_object_name = 'tasks'
    permission_required = 'task.task_management'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['form'] = TaskForm
        return context


def create(request):
    form = TaskForm(request.POST)
    if form.is_valid():
        Task.objects.create(
            title=form.cleaned_data['title'],
            description=form.cleaned_data['description'],
            assignee=form.cleaned_data['assignee']
        )
    return HttpResponseRedirect(reverse('task:index'))


def delete(request, task_id):
    Task.objects.get(pk=task_id).delete()
    return HttpResponseRedirect(reverse('task:index'))

# Create your views here.
